<!DOCTYPE html>
<?php
$current_page = 'Home';
$timestamp = date("YmdHis");
$site_path = 'http://localhost/racf_website/assets';
$site_url = 'http://localhost/racf_website/views';
// $site_path = 'https://www.creamyw.com/dev/rent/web/assets';
// $site_url = 'https://www.creamyw.com/dev/rent/web/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../components/links.php'; ?>
</head>

<body>
    <?php include '../../components/menu.php'; ?>
    <div class="wrapper">
        <div class="home__web__page">
            <header class="header d-flex flex-column justify-content-center">
                <div class="header__bg">
                    <img src="<?php echo $site_path ?>/images/header-bg.svg" class="img-fluid d-md-block d-none w-100"
                        alt="">
                    <img src="<?php echo $site_path ?>/images/header-sm.svg" class="img-fluid d-md-none d-block w-100"
                        alt="">
                </div>
                <div class="container text-white ">
                    <div class="row">
                        <div class="col-lg-5 text-lg-start text-center">
                            <div class="sm__title text-uppercase">
                                Welcome to RentaCyberFriend
                            </div>
                            <div class="bg__title my-30">
                                <h1 class="fw-bolder">
                                    Bring People Together
                                </h1>
                            </div>
                            <div class="desc">
                                <p class="mb-0">Connect anywhere, anytime</p>
                            </div>
                            <div class="mt-30 d-inline-block">
                                <a href="" class="racf__btn d-flex align-items-center">
                                    Join Us Now
                                    <img src="<?php echo $site_path ?>/images/arrow-link.svg" class="ms-10" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-10 mx-lg-none mx-auto">
                            <div class="header__mockup position-relative">
                                <div class="sm">
                                    <img src="<?php echo $site_path ?>/images/sm-call.png" alt="">
                                    <div class="call__actions">
                                        <img src="<?php echo $site_path ?>/images/call-actions.png" alt="">
                                    </div>
                                </div>
                                <div class="video__rect">
                                    <img src="<?php echo $site_path ?>/images/video-rect.png" alt="">
                                </div>
                                <div class="bg">
                                    <img src="<?php echo $site_path ?>/images/bg-call.png" alt="">
                                    <div class="call__actions">
                                        <img src="<?php echo $site_path ?>/images/call-actions.png" alt="">
                                    </div>
                                    <div class="timer">
                                        <img src="<?php echo $site_path ?>/images/timer.png" alt="">
                                    </div>
                                    <div class="earned">
                                        <img src="<?php echo $site_path ?>/images/earned.png" alt="">
                                    </div>
                                    <div class="call__sm">
                                        <img src="<?php echo $site_path ?>/images/internal-call.png" alt="">
                                        <img src="<?php echo $site_path ?>/images/voice.png" alt="" class="voice">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div class="how__it__work py-lg-60 py-md-50 py-30">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="row align-items-center">
                                <div class="col-xl-7 col-lg-6">
                                    <div class="title text-lg-start text-center">
                                        <h2>How <span>RentaCyberFriend</span> Works</h2>
                                    </div>
                                </div>
                                <div class="col-xl-5 col-lg-6">
                                    <div class="d-flex justify-content-lg-end justify-content-center mt-lg-0 mt-30">
                                        <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link active" id="pills-cyberfriend-tab"
                                                    data-bs-toggle="pill" data-bs-target="#pills-cyberfriend"
                                                    type="button" role="tab" aria-controls="pills-cyberfriend"
                                                    aria-selected="true">Be
                                                    Cyber
                                                    Friend</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" id="pills-member-tab" data-bs-toggle="pill"
                                                    data-bs-target="#pills-member" type="button" role="tab"
                                                    aria-controls="pills-member" aria-selected="false">Find Cyber
                                                    Friend</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cyberfriend__member col-12 pt-lg-50 pt-md-40 pt-30 mt-lg-0 mt-30">
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-cyberfriend" role="tabpanel"
                                    aria-labelledby="pills-cyberfriend-tab">
                                    <div class="row">
                                        <div class="col-lg-6 col-8 mx-lg-none mx-auto">
                                            <div class="cyber__friend__features">
                                                <div class="main__img text-center">
                                                    <img src="<?php echo $site_path ?>/images/main-cyber.jpg"
                                                        class="img-fluid main__cyber" alt="">
                                                    <div class="paid__time">
                                                        <img src="<?php echo $site_path ?>/images/paid-time.png" alt="">
                                                    </div>
                                                    <div class="add__hobbies">
                                                        <img src="<?php echo $site_path ?>/images/add-hobbies.png"
                                                            alt="">
                                                    </div>
                                                    <div class="search__friend">
                                                        <img src="<?php echo $site_path ?>/images/search-friend.png"
                                                            alt="">
                                                    </div>
                                                    <div class="accept__video">
                                                        <img src="<?php echo $site_path ?>/images/accept-video.png"
                                                            alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mt-lg-0 mt-md-60 mt-30">
                                            <ul class="feature__lists cyberfriend list-unstyled mb-0">
                                                <li class="feature__item p-md-20 p-15">
                                                    <div class="img">
                                                        <img src="<?php echo $site_path ?>/images/cyber-signup.svg"
                                                            alt="">
                                                    </div>
                                                    <div class="data">
                                                        <div class="heading">
                                                            <h2>Signup</h2>
                                                        </div>
                                                        <div class="desc">
                                                            Sign up with you name, email and password to get started
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="feature__item p-md-20 p-15">
                                                    <div class="img">
                                                        <img src="<?php echo $site_path ?>/images/profile-details.svg"
                                                            alt="">
                                                    </div>
                                                    <div class="data">
                                                        <div class="heading">
                                                            <h2>Profile Details</h2>
                                                        </div>
                                                        <div class="desc">
                                                            Complete you profile information ie, bio , skills & media ,
                                                            etc
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="feature__item p-md-20 p-15">
                                                    <div class="img">
                                                        <img src="<?php echo $site_path ?>/images/earning-money.svg"
                                                            alt="">
                                                    </div>
                                                    <div class="data">
                                                        <div class="heading">
                                                            <h2>Start Earning Money</h2>
                                                        </div>
                                                        <div class="desc">
                                                            When your profile is approved by our team , you can share
                                                            your
                                                            profile on
                                                            social media and ready to get follower
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-member" role="tabpanel"
                                    aria-labelledby="pills-member-tab">
                                    <div class="row">
                                        <div class="col-lg-6 order-lg-0 order-1 mt-lg-0 mt-md-60 mt-30">
                                            <ul class="feature__lists member list-unstyled mb-0">
                                                <li class="feature__item p-md-20 p-15">
                                                    <div class="img">
                                                        <img src="<?php echo $site_path ?>/images/search-friend.svg"
                                                            alt="">
                                                    </div>
                                                    <div class="data">
                                                        <div class="heading">
                                                            <h2>Search Friends</h2>
                                                        </div>
                                                        <div class="desc">
                                                            Search with age, gender & price
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="feature__item p-md-20 p-15">
                                                    <div class="img">
                                                        <img src="<?php echo $site_path ?>/images/profile-details.svg"
                                                            alt="">
                                                    </div>
                                                    <div class="data">
                                                        <div class="heading">
                                                            <h2>Profile Details</h2>
                                                        </div>
                                                        <div class="desc">
                                                            View their bio and other information
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="feature__item p-md-20 p-15">
                                                    <div class="img">
                                                        <img src="<?php echo $site_path ?>/images/make-cyber.svg"
                                                            alt="">
                                                    </div>
                                                    <div class="data">
                                                        <div class="heading">
                                                            <h2>Make Cyber Friend</h2>
                                                        </div>
                                                        <div class="desc">
                                                            When you find right person for you make him/her friend
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-6 col-8 mx-lg-none mx-auto order-lg-1 order-0">
                                            <div class="member__features">
                                                <div class="main__img text-center">
                                                    <img src="<?php echo $site_path ?>/images/main-member.jpg"
                                                        class="img-fluid main__member" alt="">
                                                    <div class="signup__profile">
                                                        <img src="<?php echo $site_path ?>/images/signup-profile.png"
                                                            alt="">
                                                    </div>
                                                    <div class="search__friend">
                                                        <img src="<?php echo $site_path ?>/images/search-friend.png"
                                                            alt="">
                                                    </div>
                                                    <div class="accept__video">
                                                        <img src="<?php echo $site_path ?>/images/accept-video.png"
                                                            alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="cyberfriend__features py-lg-60 py-md-50 py-30">
                <div class="container">
                    <div class="title text-center">
                        <h2>Get the most out of <span>RentaCyberFriend</span></h2>
                    </div>
                    <div class="desc text-center">
                        Discover why hundreds of millions people use RentCyberFrient to video call everyday
                    </div>
                    <div class="featured__items pt-lg-50 pt-md-40 pt-30">
                        <div class="row align-items-center">
                            <div class="col-lg-4 text-lg-end">
                                <ul class="list-unstyled mb-0 features__list">
                                    <li class="p-xl-30 p-lg-20 p-20">
                                        <div class="icon mb-10">
                                            <img src="<?php echo $site_path ?>/images/feature-video.svg" alt="">
                                        </div>
                                        <div class="heading mb-10">
                                            Video call and audio
                                        </div>
                                        <div class="desc">
                                            with audio and video call in rentacyberfriend, you can have stable call with
                                            your friends, No matter they are aboard
                                        </div>
                                    </li>
                                    <li class="p-xl-30 p-lg-20 p-20">
                                        <div class="icon mb-10">
                                            <img src="<?php echo $site_path ?>/images/feature-search.svg" alt="">
                                        </div>
                                        <div class="heading mb-10">
                                            Search people what you what
                                        </div>
                                        <div class="desc">
                                            search people what you want to start video call face to face, no matter
                                            where you are
                                        </div>
                                    </li>
                                    <li class="p-xl-30 p-lg-20 p-20">
                                        <div class="icon mb-10">
                                            <img src="<?php echo $site_path ?>/images/feature-payment.svg" alt="">
                                        </div>
                                        <div class="heading mb-10">
                                            Fast and guaranteed payments
                                        </div>
                                        <div class="desc">
                                            get paid fast and safety with very highy security </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-4 text-center my-lg-0 my-30">
                                <div class="mockup__iphone">
                                    <img src="<?php echo $site_path ?>/images/mockup.png" class="img-fluid mockup"
                                        alt="">
                                    <div class="call__bg">
                                        <img src="<?php echo $site_path ?>/images/call-bg.png"
                                            class="img-fluid bg__call" alt="">
                                    </div>
                                    <div class="call__actions">
                                        <img src="<?php echo $site_path ?>/images/call-actions.png" alt="">
                                    </div>
                                    <div class="call__sm">
                                        <img src="<?php echo $site_path ?>/images/internal-call.png" alt="">
                                        <img src="<?php echo $site_path ?>/images/voice.png" alt="" class="voice">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <ul class="list-unstyled mb-0 features__list">
                                    <li class="p-xl-30 p-lg-20 p-20">
                                        <div class="icon mb-10">
                                            <img src="<?php echo $site_path ?>/images/feature-private.svg" alt="">
                                        </div>
                                        <div class="heading mb-10">
                                            Private conversations
                                        </div>
                                        <div class="desc">
                                            Keep your sensitive conversations private with industry stand to end
                                            encryption </div>
                                    </li>
                                    <li class="p-xl-30 p-lg-20 p-20">
                                        <div class="icon mb-10">
                                            <img src="<?php echo $site_path ?>/images/feature-time.svg" alt="">
                                        </div>
                                        <div class="heading mb-10">
                                            Pay per call or min </div>
                                        <div class="desc">
                                            Keep your sensitive conversations private with industry stand to end
                                            encryption
                                        </div>
                                    </li>
                                    <li class="p-xl-30 p-lg-20 p-20">
                                        <div class="icon mb-10">
                                            <img src="<?php echo $site_path ?>/images/feature-share.svg" alt="">
                                        </div>
                                        <div class="heading mb-10">
                                            Share your link
                                        </div>
                                        <div class="desc">
                                            Share your link with your customers. They can click the link and instantly
                                            call you through their web browser
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="hobbies py-lg-60 py-md-50 py-30">
                <div class="container">
                    <div class="title text-center">
                        <h2>Popular <span>Hobbies</span></h2>
                    </div>
                    <div class="desc text-center">
                        Let make some new friends! Your in mood for..
                    </div>
                    <div class="hobby__items pt-lg-50 pt-md-40 pt-30">
                        <div class="row">
                            <?php
                            for ($i = 0; $i < 6; $i++) {
                                echo '
                                        <div class="col-lg-4 col-md-6 mb-30">
                                            <a href="" class="d-block hobby__item">
                                                <svg viewBox="0 0 420 211" fill="none" xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                    <mask id="shape-mask-' . $i . '">
                                                    <path id="hobby-shape" d="M0,22A22,22,0,0,1,22,0H398a22,22,0,0,1,22,22V122c0,12.15-4.142,18.438-22,22L22,211A22,22,0,0,1,0,189Z" fill="#fff"/>
                                                    </mask>
                                                        <image width="100%" height="100%" preserveAspectRatio="xMidYMid slice"
                                                            xlink:href="' . $site_path . '/images/hobby-item.jpg"
                                                mask="url(#shape-mask-' . $i . ')"></image>
                                                </svg>
                                                <span class="hobby__name">#Hobby Name</span>
                                            </a>
                                        </div>
                                    ';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="text-center ">
                        <div class="d-inline-block">
                            <a href="" class="racf__btn d-flex align-items-center">
                                See More
                                <img src="<?php echo $site_path ?>/images/arrow-link.svg" class="ms-10 svg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '../../components/footer.php'; ?>
    <?php include '../../components/scripts.php'; ?>
</body>

</html>