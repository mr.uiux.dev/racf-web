<!DOCTYPE html>
<?php
$current_page = 'Search';
$timestamp = date("YmdHis");
$site_path = 'http://localhost/racf_website/assets';
$site_url = 'http://localhost/racf_website/views';
// $site_path = 'https://www.creamyw.com/dev/rent/web/assets';
// $site_url = 'https://www.creamyw.com/dev/rent/web/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../components/links.php'; ?>
</head>

<body>
    <?php include '../../components/menu.php'; ?>
    <div class="wrapper">
        <div class="general__web__page">
            <div class="block">
                <div class="container">
                    <div class="title text-center">
                        <h2>Terms <span>&</span> Condition</h2>
                    </div>
                    <form action="" class="mt-lg-50 mt-md-40 mt-30">
                        <div class="card p-lg-25 p-15 mt-md-30 mt-20">
                            <?php 
                               for ($i=0; $i < 4; $i++) { 
                                echo '
                                <div class="data">
                                    <div class="heading">
                                        Heading Name Here
                                    </div>
                                    <div class="data">
                                        <p>
                                        This Terms of Service (the “Terms”) describes the rights and responsibilities that apply to your use of Dribbble’s websites, services, and mobile app (collectively, the “Service”), each owned and operated by Dribbble Holdings Ltd. (“Dribbble”, “we”, “our” or “us”).</p>
                                        <p>
                                        Please read the Terms carefully before using the Service. If you don’t agree to the Terms, as well as Dribbble’s Privacy Policy (the “Privacy Policy”) and Dribbble’s Community Guidelines (the “Community Guidelines”), you may not use the Service. If you are entering into the Terms on behalf of a company or other legal entity, you represent that you have the authority to bind such entity to the Terms. If you do not have such authority, you must not accept the Terms or use the Service on behalf of such entity. The Service is only available to you if you have entered the age of majority in your jurisdiction of residence and are fully able and competent to enter into, abide by and comply with the Terms.
                                        </p>
                                        </div>
                                </div>
                            ';
                               }
                            ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- <?php include '../../components/footer.php'; ?> -->
    <?php include '../../components/sm-footer.php'; ?>
    <?php include '../../components/scripts.php'; ?>
</body>

</html>