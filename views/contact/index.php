<!DOCTYPE html>
<?php 
    $current_page = 'Contact'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf_website/assets';
    $site_url = 'http://localhost/racf_website/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/web/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/web/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../components/links.php'; ?>
</head>

<body>
    <?php include '../../components/menu.php'; ?>
    <div class="wrapper">
        <div class="contact__web__page">
            <div class="container">
                <div class="title text-center">
                    <h2>Contact <span>Us</span></h2>
                </div>
                <div class="desc text-center w-75 mx-auto">
                    RACF, INC. through its service, rentacyberfriend.com, collects and processes the data you enter
                    in the form, which is necessary in order to communicate with you and resolve any questions you
                    may have. For more information on the processing of your personal data and your rights, read the
                    <a href="">Terms</a> and <a href="">Privacy Policy</a>.
                </div>
                <div class="row align-items-center mt-lg-50 mt-md-40 mt-30">
                    <div class="col-lg-6">
                        <form action="" class="row p-lg-40 p-md-30 p-20">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" name="" id="" placeholder="First Name">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" name="" id="" placeholder="Last Name">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="tel" class="form-control" name="" id="" placeholder="Telephone Number">
                            </div>
                            <div class="form-group col">
                                <select class="form-control" id="country" name="country">
                                    <option value="" selected="selected">Select your Country</option>
                                    <option value="1">Afghanistan</option>
                                    <option value="2">Albania</option>
                                    <option value="3">Algeria</option>
                                    <option value="4">American Samoa</option>
                                    <option value="5">Andorra</option>
                                    <option value="6">Angola</option>
                                    <option value="7">Anguilla</option>
                                    <option value="8">Antarctica</option>
                                    <option value="9">Antigua and Barbuda</option>
                                    <option value="10">Argentina</option>
                                    <option value="11">Armenia</option>
                                    <option value="12">Aruba</option>
                                    <option value="13">Australia</option>
                                    <option value="14">Austria</option>
                                    <option value="15">Azerbaijan</option>
                                    <option value="16">Bahamas</option>
                                    <option value="17">Bahrain</option>
                                    <option value="18">Bangladesh</option>
                                    <option value="19">Barbados</option>
                                    <option value="20">Belarus</option>
                                    <option value="21">Belgium</option>
                                    <option value="22">Belize</option>
                                    <option value="23">Benin</option>
                                    <option value="24">Bermuda</option>
                                    <option value="25">Bhutan</option>
                                    <option value="26">Bolivia</option>
                                    <option value="27">Bosnia and Herzegovina</option>
                                    <option value="28">Botswana</option>
                                    <option value="29">Bouvet Island</option>
                                    <option value="30">Brazil</option>
                                    <option value="31">British Indian Ocean Territory</option>
                                    <option value="32">Brunei Darussalam</option>
                                    <option value="33">Bulgaria</option>
                                    <option value="34">Burkina Faso</option>
                                    <option value="35">Burundi</option>
                                    <option value="36">Cambodia</option>
                                    <option value="37">Cameroon</option>
                                    <option value="38">Canada</option>
                                    <option value="39">Cape Verde</option>
                                    <option value="40">Cayman Islands</option>
                                    <option value="41">Central African Republic</option>
                                    <option value="42">Chad</option>
                                    <option value="43">Chile</option>
                                    <option value="44">China</option>
                                    <option value="45">Christmas Island</option>
                                    <option value="46">Cocos (Keeling) Islands</option>
                                    <option value="47">Colombia</option>
                                    <option value="48">Comoros</option>
                                    <option value="49">Congo</option>
                                    <option value="50">Congo, The Democratic Republic of The</option>
                                    <option value="51">Cook Islands</option>
                                    <option value="52">Costa Rica</option>
                                    <option value="53">Cote D'ivoire</option>
                                    <option value="54">Croatia</option>
                                    <option value="55">Cuba</option>
                                    <option value="56">Cyprus</option>
                                    <option value="57">Czech Republic</option>
                                    <option value="58">Denmark</option>
                                    <option value="59">Djibouti</option>
                                    <option value="60">Dominica</option>
                                    <option value="61">Dominican Republic</option>
                                    <option value="62">Ecuador</option>
                                    <option value="63">Egypt</option>
                                    <option value="64">El Salvador</option>
                                    <option value="65">Equatorial Guinea</option>
                                    <option value="66">Eritrea</option>
                                    <option value="67">Estonia</option>
                                    <option value="68">Ethiopia</option>
                                    <option value="69">Falkland Islands (Malvinas)</option>
                                    <option value="70">Faroe Islands</option>
                                    <option value="71">Fiji</option>
                                    <option value="72">Finland</option>
                                    <option value="73">France</option>
                                    <option value="74">French Guiana</option>
                                    <option value="75">French Polynesia</option>
                                    <option value="76">French Southern Territories</option>
                                    <option value="77">Gabon</option>
                                    <option value="78">Gambia</option>
                                    <option value="79">Georgia</option>
                                    <option value="80">Germany</option>
                                    <option value="81">Ghana</option>
                                    <option value="82">Gibraltar</option>
                                    <option value="83">Greece</option>
                                    <option value="84">Greenland</option>
                                    <option value="85">Grenada</option>
                                    <option value="86">Guadeloupe</option>
                                    <option value="87">Guam</option>
                                    <option value="88">Guatemala</option>
                                    <option value="89">Guinea</option>
                                    <option value="90">Guinea-bissau</option>
                                    <option value="91">Guyana</option>
                                    <option value="92">Haiti</option>
                                    <option value="93">Heard Island and Mcdonald Islands</option>
                                    <option value="94">Holy See (Vatican City State)</option>
                                    <option value="95">Honduras</option>
                                    <option value="96">Hong Kong</option>
                                    <option value="97">Hungary</option>
                                    <option value="98">Iceland</option>
                                    <option value="99">India</option>
                                    <option value="100">Indonesia</option>
                                    <option value="101">Iran, Islamic Republic of</option>
                                    <option value="102">Iraq</option>
                                    <option value="103">Ireland</option>
                                    <option value="104">Israel</option>
                                    <option value="105">Italy</option>
                                    <option value="106">Jamaica</option>
                                    <option value="107">Japan</option>
                                    <option value="108">Jordan</option>
                                    <option value="109">Kazakhstan</option>
                                    <option value="110">Kenya</option>
                                    <option value="111">Kiribati</option>
                                    <option value="112">Korea, Democratic People's Republic of</option>
                                    <option value="113">Korea, Republic of</option>
                                    <option value="114">Kuwait</option>
                                    <option value="115">Kyrgyzstan</option>
                                    <option value="116">Lao People's Democratic Republic</option>
                                    <option value="117">Latvia</option>
                                    <option value="118">Lebanon</option>
                                    <option value="119">Lesotho</option>
                                    <option value="120">Liberia</option>
                                    <option value="121">Libyan Arab Jamahiriya</option>
                                    <option value="122">Liechtenstein</option>
                                    <option value="123">Lithuania</option>
                                    <option value="124">Luxembourg</option>
                                    <option value="125">Macao</option>
                                    <option value="126">Fyrom</option>
                                    <option value="127">Madagascar</option>
                                    <option value="128">Malawi</option>
                                    <option value="129">Malaysia</option>
                                    <option value="130">Maldives</option>
                                    <option value="131">Mali</option>
                                    <option value="132">Malta</option>
                                    <option value="133">Marshall Islands</option>
                                    <option value="134">Martinique</option>
                                    <option value="135">Mauritania</option>
                                    <option value="136">Mauritius</option>
                                    <option value="137">Mayotte</option>
                                    <option value="138">Mexico</option>
                                    <option value="139">Micronesia, Federated States of</option>
                                    <option value="140">Moldova, Republic of</option>
                                    <option value="141">Monaco</option>
                                    <option value="142">Mongolia</option>
                                    <option value="143">Montserrat</option>
                                    <option value="144">Morocco</option>
                                    <option value="145">Mozambique</option>
                                    <option value="146">Myanmar</option>
                                    <option value="147">Namibia</option>
                                    <option value="148">Nauru</option>
                                    <option value="149">Nepal</option>
                                    <option value="150">Netherlands</option>
                                    <option value="151">Netherlands Antilles</option>
                                    <option value="152">New Caledonia</option>
                                    <option value="153">New Zealand</option>
                                    <option value="154">Nicaragua</option>
                                    <option value="155">Niger</option>
                                    <option value="156">Nigeria</option>
                                    <option value="157">Niue</option>
                                    <option value="158">Norfolk Island</option>
                                    <option value="159">Northern Mariana Islands</option>
                                    <option value="160">Norway</option>
                                    <option value="161">Oman</option>
                                    <option value="162">Pakistan</option>
                                    <option value="163">Palau</option>
                                    <option value="164">Palestinian Territory, Occupied</option>
                                    <option value="165">Panama</option>
                                    <option value="166">Papua New Guinea</option>
                                    <option value="167">Paraguay</option>
                                    <option value="168">Peru</option>
                                    <option value="169">Philippines</option>
                                    <option value="170">Pitcairn</option>
                                    <option value="171">Poland</option>
                                    <option value="172">Portugal</option>
                                    <option value="173">Puerto Rico</option>
                                    <option value="174">Qatar</option>
                                    <option value="175">Reunion</option>
                                    <option value="176">Romania</option>
                                    <option value="177">Russian Federation</option>
                                    <option value="178">Rwanda</option>
                                    <option value="179">Saint Helena</option>
                                    <option value="180">Saint Kitts and Nevis</option>
                                    <option value="181">Saint Lucia</option>
                                    <option value="182">Saint Pierre and Miquelon</option>
                                    <option value="183">Saint Vincent and The Grenadines</option>
                                    <option value="184">Samoa</option>
                                    <option value="185">San Marino</option>
                                    <option value="186">Sao Tome and Principe</option>
                                    <option value="187">Saudi Arabia</option>
                                    <option value="188">Senegal</option>
                                    <option value="189">Serbia and Montenegro</option>
                                    <option value="190">Seychelles</option>
                                    <option value="191">Sierra Leone</option>
                                    <option value="192">Singapore</option>
                                    <option value="193">Slovakia</option>
                                    <option value="194">Slovenia</option>
                                    <option value="195">Solomon Islands</option>
                                    <option value="196">Somalia</option>
                                    <option value="197">South Africa</option>
                                    <option value="198">South Georgia and The South Sandwich Islands</option>
                                    <option value="199">Spain</option>
                                    <option value="200">Sri Lanka</option>
                                    <option value="201">Sudan</option>
                                    <option value="202">Suriname</option>
                                    <option value="203">Svalbard and Jan Mayen</option>
                                    <option value="204">Swaziland</option>
                                    <option value="205">Sweden</option>
                                    <option value="206">Switzerland</option>
                                    <option value="207">Syrian Arab Republic</option>
                                    <option value="208">Taiwan, Province of China</option>
                                    <option value="209">Tajikistan</option>
                                    <option value="210">Tanzania, United Republic of</option>
                                    <option value="211">Thailand</option>
                                    <option value="212">Timor-leste</option>
                                    <option value="213">Togo</option>
                                    <option value="214">Tokelau</option>
                                    <option value="215">Tonga</option>
                                    <option value="216">Trinidad and Tobago</option>
                                    <option value="217">Tunisia</option>
                                    <option value="218">Turkey</option>
                                    <option value="219">Turkmenistan</option>
                                    <option value="220">Turks and Caicos Islands</option>
                                    <option value="221">Tuvalu</option>
                                    <option value="222">Uganda</option>
                                    <option value="223">Ukraine</option>
                                    <option value="224">United Arab Emirates</option>
                                    <option value="225">United Kingdom</option>
                                    <option value="226">United States</option>
                                    <option value="227">United States Minor Outlying Islands</option>
                                    <option value="228">Uruguay</option>
                                    <option value="229">Uzbekistan</option>
                                    <option value="230">Vanuatu</option>
                                    <option value="231">Venezuela</option>
                                    <option value="232">Viet Nam</option>
                                    <option value="233">Virgin Islands, British</option>
                                    <option value="234">Virgin Islands, U.S.</option>
                                    <option value="235">Wallis and Futuna</option>
                                    <option value="236">Western Sahara</option>
                                    <option value="237">Yemen</option>
                                    <option value="238">Zambia</option>
                                    <option value="239">Zimbabwe</option>
                                </select>
                            </div>
                            <div class="form-group col-12">
                                <textarea name="" class="form-control" placeholder="Additional Information" id=""
                                    rows="10"></textarea>
                            </div>
                            <div class="text-end col-12">
                                <div class="d-inline-block">
                                    <button type="submit" class="racf__btn d-flex align-items-center">
                                        Send
                                        <img src="<?php echo $site_path ?>/images/arrow-link.svg" class="ms-10 svg"
                                            alt="">
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-5 offset-lg-1 mt-lg-0 mt-30">
                        <ul class="list-unstyled links mb-0">
                            <li>
                                <a href="mailto:info@rentacyberfriend.com">
                                    <span class="fw-bold"><i class="fa-fw fa fa-envelope"></i> </span>
                                    info@rentacyberfriend.com
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="fw-bold"><i class="fa-fw fa fa-facebook"></i> </span>
                                    Follow us on Facebook
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="fw-bold"><i class="fa-fw fa fa-twitter"></i> </span>
                                    Follow us on Twitter
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="fw-bold"><i class="fa-fw fa fa-instagram"></i> </span>
                                    Follow us on Instagram
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="fw-bold"><i class="fa fa-tripadvisor"></i> </span>
                                    Read our reviews on Tripadvisor
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '../../components/footer.php'; ?>
    <?php include '../../components/scripts.php'; ?>
</body>

</html>