<!DOCTYPE html>
<?php 
    $current_page = 'How it Work'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf_website/assets';
    $site_url = 'http://localhost/racf_website/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/web/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/web/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../components/links.php'; ?>
</head>

<body>
    <?php include '../../components/menu.php'; ?>
    <div class="wrapper">
        <div class="how__web__page">
            <div class="cyber__friend">
                <div class="container">
                    <div class="title text-center">
                        <h1>Signup & <span>Become</span> A Cyber Friend</h1>
                    </div>
                    <div class="row align-items-center py-lg-50 p-md-40 p-30">
                        <div class="col-lg-6 col-8 mx-lg-none mx-auto text-start">
                            <div class="cyber__friend__features">
                                <div class="main__img text-center">
                                    <img src="<?php echo $site_path ?>/images/main-cyber.jpg"
                                        class="img-fluid main__cyber" alt="">
                                    <div class="paid__time">
                                        <img src="<?php echo $site_path ?>/images/paid-time.png" alt="">
                                    </div>
                                    <div class="add__hobbies">
                                        <img src="<?php echo $site_path ?>/images/add-hobbies.png" alt="">
                                    </div>
                                    <div class="search__friend">
                                        <img src="<?php echo $site_path ?>/images/search-friend.png" alt="">
                                    </div>
                                    <div class="accept__video">
                                        <img src="<?php echo $site_path ?>/images/accept-video.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mt-lg-0 mt-md-60 mt-30">
                            <ul class="feature__lists list-unstyled mb-0">
                                <li class="feature__item p-md-20 p-15">
                                    <div class="img">
                                        <img src="<?php echo $site_path ?>/images/cyber-signup.svg" alt="">
                                    </div>
                                    <div class="data">
                                        <div class="heading">
                                            <h2>Signup</h2>
                                        </div>
                                        <div class="desc">
                                            Sign up with you name, email and password to get started
                                        </div>
                                    </div>
                                </li>
                                <li class="feature__item p-md-20 p-15">
                                    <div class="img">
                                        <img src="<?php echo $site_path ?>/images/profile-details.svg" alt="">
                                    </div>
                                    <div class="data">
                                        <div class="heading">
                                            <h2>Profile Details</h2>
                                        </div>
                                        <div class="desc">
                                            Complete you profile information ie, bio , skills & media , etc
                                        </div>
                                    </div>
                                </li>
                                <li class="feature__item p-md-20 p-15">
                                    <div class="img">
                                        <img src="<?php echo $site_path ?>/images/earning-money.svg" alt="">
                                    </div>
                                    <div class="data">
                                        <div class="heading">
                                            <h2>Start Earning Money</h2>
                                        </div>
                                        <div class="desc">
                                            When your profile is approved by our team , you can share your profile on
                                            social media and ready to get follower
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="member mt-lg-50 mt-md-40 mt-30">
                <div class="container">
                    <div class="title text-center">
                        <h1>Signup & <span>Rent</span> A Cyber Friend</h1>
                    </div>
                    <div class="row align-items-center py-lg-50 p-md-40 p-30">
                        <div class="col-lg-6 order-lg-0 order-1 mt-lg-0 mt-md-60 mt-30">
                            <ul class="feature__lists list-unstyled mb-0">
                                <li class="feature__item p-md-20 p-15">
                                    <div class="img">
                                        <img src="<?php echo $site_path ?>/images/search-friend.svg" alt="">
                                    </div>
                                    <div class="data">
                                        <div class="heading">
                                            <h2>Search Friends</h2>
                                        </div>
                                        <div class="desc">
                                            Search with age, gender & price
                                        </div>
                                    </div>
                                </li>
                                <li class="feature__item p-md-20 p-15">
                                    <div class="img">
                                        <img src="<?php echo $site_path ?>/images/profile-details.svg" alt="">
                                    </div>
                                    <div class="data">
                                        <div class="heading">
                                            <h2>Profile Details</h2>
                                        </div>
                                        <div class="desc">
                                            View their bio and other information
                                        </div>
                                    </div>
                                </li>
                                <li class="feature__item p-md-20 p-15">
                                    <div class="img">
                                        <img src="<?php echo $site_path ?>/images/make-cyber.svg" alt="">
                                    </div>
                                    <div class="data">
                                        <div class="heading">
                                            <h2>Make Cyber Friend</h2>
                                        </div>
                                        <div class="desc">
                                            When you find right person for you make him/her friend
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6 col-8 mx-lg-none mx-auto order-lg-1 order-0">
                            <div class="member__features">
                                <div class="main__img text-center">
                                    <img src="<?php echo $site_path ?>/images/main-member.jpg"
                                        class="img-fluid main__member" alt="">
                                    <div class="signup__profile">
                                        <img src="<?php echo $site_path ?>/images/signup-profile.png" alt="">
                                    </div>
                                    <div class="search__friend">
                                        <img src="<?php echo $site_path ?>/images/search-friend.png" alt="">
                                    </div>
                                    <div class="accept__video">
                                        <img src="<?php echo $site_path ?>/images/accept-video.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '../../components/footer.php'; ?>
    <?php include '../../components/scripts.php'; ?>
</body>

</html>