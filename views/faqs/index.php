<!DOCTYPE html>
<?php 
    $current_page = 'FAQs'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf_website/assets';
    $site_url = 'http://localhost/racf_website/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/web/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/web/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../components/links.php'; ?>
</head>

<body>
    <?php include '../../components/menu.php'; ?>
    <div class="wrapper">
        <div class="faqs__web__page">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-4 mb-lg-0 mb-30">
                        <div class="data text-lg-left text-center">
                            <div class="title">
                                <h1>Fraquently Asked <span>Questions</span></h1>
                            </div>
                            <div class="img mt-30">
                                <img src="<?php echo $site_path ?>/images/faq-bg.svg" class="img-fluid" alt="">
                            </div>
                            <div class="desc mt-30">
                                Can’t find what you were looking for? Please, send us an <a href="#">email</a>.
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-8">
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        How does your service work?
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show"
                                    aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div class="accordion-body p-md-15 p-10">
                                        <div class="ratio ratio-16x9">
                                            <iframe src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0"
                                                title="YouTube video" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                               for ($i=2; $i < 15; $i++) { 
                                   echo '
                                   <div class="accordion-item">
                                        <h2 class="accordion-header" id="heading'.$i.'">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse'.$i.'" aria-expanded="false" aria-controls="collapse'.$i.'">
                                                Accordion Item #2
                                            </button>
                                        </h2>
                                        <div id="collapse'.$i.'" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                            <div class="accordion-body">
                                                <p>
                                                Paidvideocall.com is a browser-based solution for paid video and audio calls. You or your clients do not need to download an application to make a call.
                                                </p>
                                                <p>
                                                Instead of a phone number, you get a personal link that you can share with your customers. When a customer clicks the link, a call session is initiated between you and your client. The client is prompted for payment before the call begins.
                                                </p>
                                                <p>
                                                We offer features to make your life easier. Such as payment functionality, booking capabilities, and a completely secure and encrypted cloud service.
                                                </p>
                                                <p>
                                                Our service is very easy to use and will get you started to accept calls from your customers in minutes.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                   ';
                               }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '../../components/footer.php'; ?>
    <?php include '../../components/scripts.php'; ?>
</body>

</html>