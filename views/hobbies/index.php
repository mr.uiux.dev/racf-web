<!DOCTYPE html>
<?php 
    $current_page = 'Hobbies'; 
    $timestamp = date("YmdHis");
    $site_path = 'http://localhost/racf_website/assets';
    $site_url = 'http://localhost/racf_website/views';
    // $site_path = 'https://www.creamyw.com/dev/rent/web/assets';
    // $site_url = 'https://www.creamyw.com/dev/rent/web/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../components/links.php'; ?>
</head>

<body>
    <?php include '../../components/menu.php'; ?>
    <div class="wrapper">
        <div class="hobby__web__page">
            <div class="hobbies">
                <div class="container">
                    <div class="title text-center">
                        <h2>Popular <span>Hobbies</span></h2>
                    </div>
                    <div class="desc text-center">
                        Let make some new friends! Your in mood for..
                    </div>
                    <div class="hobby__items pt-lg-50 pt-md-40 pt-30">
                        <div class="row">
                            <?php
                                for ($i=0; $i < 12; $i++) { 
                                    echo '
                                        <div class="col-lg-4 col-md-6 mb-30">
                                            <a href="" class="d-block hobby__item">
                                                <svg viewBox="0 0 420 211" fill="none" xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                    <mask id="shape-mask-'.$i.'">
                                                    <path id="hobby-shape" d="M0,22A22,22,0,0,1,22,0H398a22,22,0,0,1,22,22V122c0,12.15-4.142,18.438-22,22L22,211A22,22,0,0,1,0,189Z" fill="#fff"/>
                                                    </mask>
                                                        <image width="100%" height="100%" preserveAspectRatio="xMidYMid slice"
                                                            xlink:href="'.$site_path.'/images/hobby-item.jpg"
                                                mask="url(#shape-mask-'.$i.')"></image>
                                                </svg>
                                                <span class="hobby__name">#Hobby Name</span>
                                            </a>
                                        </div>
                                    ';
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '../../components/footer.php'; ?>
    <?php include '../../components/scripts.php'; ?>
</body>

</html>