<!DOCTYPE html>
<?php
$current_page = 'Login';
$timestamp = date("YmdHis");
$site_path = 'http://localhost/racf_website/assets';
$site_url = 'http://localhost/racf_website/views';
// $site_path = 'https://www.creamyw.com/dev/rent/web/assets';
// $site_url = 'https://www.creamyw.com/dev/rent/web/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../components/links.php'; ?>
</head>

<body>
    <?php include '../../components/menu.php'; ?>
    <div class="wrapper">
        <div class="contact__web__page">
            <div class="container">
                <div class="title text-center">
                    <h2>BRINGING <span>PEOPLE</span> TOGETHER</h2>
                </div>
                <div class="desc text-center w-75 mx-auto">
                    Voluptatem Sequi Nesciunt. Neque Porro Quisquam Est, Qui Dolorem Ipsum Quia Dolor Sit Amet, Consect
                    Ipsum Quia Dolor Sit Amet.
                </div>
                <div class="row align-items-center mt-lg-50 mt-md-40 mt-30">
                    <div class="col-lg-6">
                        <div class="img">
                            <img src="<?php echo $site_path ?>/images/login-bg.svg" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 mt-lg-0 mt-30">
                        <form action="">
                            <div class="p-lg-40 p-md-30 p-20">
                                <div class="row">
                                    <div class="form-group col-12">
                                        <input type="email" class="form-control" name="" id=""
                                            placeholder="Email Address">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="password" class="form-control" name="" id=""
                                            placeholder="Passowrd">
                                    </div>
                                    <div class="form-group col-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value=""
                                                        id="flexCheckDefault">
                                                    <label class="form-check-label" for="flexCheckDefault">
                                                        Remeber Me
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="forget__password text-end">
                                                    <a href="">Forget Passowrd?</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-end col-12">
                                        <div class="d-inline-block">
                                            <button type="submit" class="racf__btn d-flex align-items-center">
                                                Login
                                                <img src="<?php echo $site_path ?>/images/arrow-link.svg"
                                                    class="ms-10 svg" alt="">
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                        <div class="create__account mt-30 text-center">
                            Don't have Account? <a href="">Create an ccount</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php include '../../components/footer.php'; ?>
    <?php include '../../components/scripts.php'; ?>
</body>

</html>