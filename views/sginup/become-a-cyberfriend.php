<!DOCTYPE html>
<?php
$current_page = 'Signup';
$timestamp = date("YmdHis");
$site_path = 'http://localhost/racf_website/assets';
$site_url = 'http://localhost/racf_website/views';
// $site_path = 'https://www.creamyw.com/dev/rent/web/assets';
// $site_url = 'https://www.creamyw.com/dev/rent/web/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../components/links.php'; ?>
</head>

<body>
    <?php include '../../components/menu.php'; ?>
    <div class="wrapper">
        <div class="contact__web__page signup__Web__page cyber__web__page">
            <div class="welcome pt-lg-60 pb-0">
                <div class="container">
                    <div class="title text-center">
                        <h2>Welcome, <span>Cyberfriends!</span></h2>
                    </div>
                    <div class="desc text-center w-75 mx-auto">
                        Are you passionate about anything and looking for ways to earn money on the side? Then, Rent A
                        Cyberfriend is for you! Receive video calls from callers who want you to engage them on what
                        matters to you both
                    </div>
                </div>
                <div class="img text-center">
                    <img src="<?php echo $site_path ?>/images/cyber-bg.png" class="img-fluid" alt="">
                </div>
            </div>
            <div class="earn py-lg-60 py-md-50 py-30 bg-white">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <div class="title">
                                <h2>Earn From Anywhere in <span>The World</span></h2>
                            </div>
                            <div class="desc mt-30">
                                <p> Get Started - Sign up, update your profile and wait for the approval.</p>
                                <p> Interests – Add your interests using different hashtags and pay rates. Callers
                                    find
                                    you by interest.</p>
                                <p> Earn Money – Be online to accept call requests and start earning money.</p>
                                <p> Grow - Share on social media to attract more clients and followers</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="img text-center">
                                <img src="<?php echo $site_path ?>/images/register.png" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="requirement py-lg-60 py-md-50 py-30">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-6 order-md-0 order-1">
                            <div class="img text-center">
                                <img src="<?php echo $site_path ?>/images/requirement.png" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-md-6 order-md-1 order-0 mt-md-0 mt-30">
                            <div class="title">
                                <h2>Cyberfriend <span>Requirements</span></h2>
                            </div>
                            <div class="desc mt-30">
                                <p> An opportunity for everyone</p>
                                <p> No degree or certificates are needed</p>
                                <p> You must have a good internet connection and a camera</p>
                                <p> Be passionate about what you do</p>
                                <p> Be 18 years and above </p>
                                <p> Be 18 years and above </p>
                                <p> Have a good smile and be friendly.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="callers bg-white py-lg-60 py-md-50 py-30">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-6">
                            <div class="title">
                                <h2>Make Deep Connections with Callers… and <span>Get Paid!</span></h2>
                            </div>
                            <div class="desc mt-30">
                                <p> Instant Video Calls </p>
                                <p> Private and Secure Conversations with end-to-end encryption</p>
                                <p> You must have a good internet connection and a camera</p>
                                <p> Payment is per minute with a 20% platform fee.</p>
                                <p> Instant Withdrawals to Your Bank Account </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="img text-center">
                                <img src="<?php echo $site_path ?>/images/callers.png" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form pt-lg-60 pt-md-50 pt-30">
                <div class="container">
                    <form action="">
                        <div class=" p-lg-40 p-md-30 p-20">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" name="" id="" placeholder="Your Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="email" class="form-control" name="" id="" placeholder="Email Address">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="password" class="form-control" name="" id="" placeholder="Passowrd">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="password" class="form-control" name="" id=""
                                        placeholder="Confirm Passowrd">
                                </div>
                                <div class="form-group col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            I accept <a href="">terms & conditions</a> and <a href="">privacy
                                                policy</a>
                                        </label>
                                    </div>
                                </div>
                                <div class="text-end col-12">
                                    <div class="d-inline-block">
                                        <button type="submit" class="racf__btn d-flex align-items-center">
                                            Signup Now
                                            <img src="<?php echo $site_path ?>/images/arrow-link.svg" class="ms-10 svg"
                                                alt="">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="have__account mt-30 text-center">
                        Already have an Account? <a href="">Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '../../components/footer.php'; ?>
    <?php include '../../components/scripts.php'; ?>
</body>

</html>