<!DOCTYPE html>
<?php
$current_page = 'Signup';
$timestamp = date("YmdHis");
$site_path = 'http://localhost/racf_website/assets';
$site_url = 'http://localhost/racf_website/views';
// $site_path = 'https://www.creamyw.com/dev/rent/web/assets';
// $site_url = 'https://www.creamyw.com/dev/rent/web/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../components/links.php'; ?>
</head>

<body>
    <?php include '../../components/menu.php'; ?>
    <div class="wrapper">
        <div class="contact__web__page signup__Web__page">
            <div class="how__it__work py-lg-60 py-md-50 py-30">
                <div class="container">
                    <div class="title text-center">
                        <h2>BRINGING <span>PEOPLE</span> TOGETHER</h2>
                    </div>
                    <div class="desc text-center w-75 mx-auto">
                        Voluptatem Sequi Nesciunt. Neque Porro Quisquam Est, Qui Dolorem Ipsum Quia Dolor Sit Amet,
                        Consect Ipsum Quia Dolor Sit Amet.
                    </div>
                    <div class="d-flex justify-content-center mt-30">
                        <ul class="nav nav-pills" id="pills-tab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="pills-cyberfriend-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-cyberfriend" type="button" role="tab"
                                    aria-controls="pills-cyberfriend" aria-selected="true">Be
                                    Cyber
                                    Friend</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-member-tab" data-bs-toggle="pill"
                                    data-bs-target="#pills-member" type="button" role="tab" aria-controls="pills-member"
                                    aria-selected="false">Find Cyber
                                    Friend</button>
                            </li>
                        </ul>
                    </div>
                    <div class="cyberfriend__member col-12 pt-lg-50 pt-md-40 pt-30 mt-lg-0 mt-30">
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-cyberfriend" role="tabpanel"
                                aria-labelledby="pills-cyberfriend-tab">
                                <div class="row align-items-center">
                                    <div class="col-lg-6 mx-lg-none mx-auto">
                                        <form action="">
                                            <div class=" p-lg-40 p-md-30 p-20">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <input type="text" class="form-control" name="" id=""
                                                            placeholder="Your Name">
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <input type="email" class="form-control" name="" id=""
                                                            placeholder="Email Address">
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <input type="password" class="form-control" name="" id=""
                                                            placeholder="Passowrd">
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <input type="password" class="form-control" name="" id=""
                                                            placeholder="Confirm Passowrd">
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" value=""
                                                                id="flexCheckDefault">
                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                I accept <a href="">terms & conditions</a> and <a
                                                                    href="">privacy policy</a>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="text-end col-12">
                                                        <div class="d-inline-block">
                                                            <button type="submit"
                                                                class="racf__btn d-flex align-items-center">
                                                                Signup Now
                                                                <img src="<?php echo $site_path ?>/images/arrow-link.svg"
                                                                    class="ms-10 svg" alt="">
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="have__account mt-30 text-center">
                                            Already have an Account? <a href="">Login</a>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 offset-lg-1 mt-lg-0 mt-md-60 mt-30">
                                        <ul class="feature__lists cyberfriend list-unstyled mb-0">
                                            <li class="feature__item p-md-20 p-15">
                                                <div class="img">
                                                    <img src="<?php echo $site_path ?>/images/cyber-signup.svg" alt="">
                                                </div>
                                                <div class="data">
                                                    <div class="heading">
                                                        <h2>Signup</h2>
                                                    </div>
                                                    <div class="desc">
                                                        Sign up with you name, email and password to get started
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="feature__item p-md-20 p-15">
                                                <div class="img">
                                                    <img src="<?php echo $site_path ?>/images/profile-details.svg"
                                                        alt="">
                                                </div>
                                                <div class="data">
                                                    <div class="heading">
                                                        <h2>Profile Details</h2>
                                                    </div>
                                                    <div class="desc">
                                                        Complete you profile information ie, bio , skills & media ,
                                                        etc
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="feature__item p-md-20 p-15">
                                                <div class="img">
                                                    <img src="<?php echo $site_path ?>/images/earning-money.svg" alt="">
                                                </div>
                                                <div class="data">
                                                    <div class="heading">
                                                        <h2>Start Earning Money</h2>
                                                    </div>
                                                    <div class="desc">
                                                        When your profile is approved by our team , you can share
                                                        your
                                                        profile on
                                                        social media and ready to get follower
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pills-member" role="tabpanel"
                                aria-labelledby="pills-member-tab">
                                <div class="row align-items-center">
                                    <div class="col-lg-5 mt-lg-0 order-lg-0 order-1 mt-md-60 mt-30">
                                        <ul class="feature__lists member list-unstyled mb-0">
                                            <li class="feature__item p-md-20 p-15">
                                                <div class="img">
                                                    <img src="<?php echo $site_path ?>/images/search-friend.svg" alt="">
                                                </div>
                                                <div class="data">
                                                    <div class="heading">
                                                        <h2>Search Friends</h2>
                                                    </div>
                                                    <div class="desc">
                                                        Search with age, gender & price
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="feature__item p-md-20 p-15">
                                                <div class="img">
                                                    <img src="<?php echo $site_path ?>/images/profile-details.svg"
                                                        alt="">
                                                </div>
                                                <div class="data">
                                                    <div class="heading">
                                                        <h2>Profile Details</h2>
                                                    </div>
                                                    <div class="desc">
                                                        View their bio and other information
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="feature__item p-md-20 p-15">
                                                <div class="img">
                                                    <img src="<?php echo $site_path ?>/images/make-cyber.svg" alt="">
                                                </div>
                                                <div class="data">
                                                    <div class="heading">
                                                        <h2>Make Cyber Friend</h2>
                                                    </div>
                                                    <div class="desc">
                                                        When you find right person for you make him/her friend
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-1 d-md-none"></div>
                                    <div class="col-lg-6 mx-lg-none order-lg-1 order-0">
                                        <form action="">
                                            <div class=" p-lg-40 p-md-30 p-20">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <input type="text" class="form-control" name="" id=""
                                                            placeholder="Your Name">
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <input type="email" class="form-control" name="" id=""
                                                            placeholder="Email Address">
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <input type="password" class="form-control" name="" id=""
                                                            placeholder="Passowrd">
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <input type="password" class="form-control" name="" id=""
                                                            placeholder="Confirm Passowrd">
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" value=""
                                                                id="flexCheckDefault">
                                                            <label class="form-check-label" for="flexCheckDefault">
                                                                I accept <a href="">terms & conditions</a> and <a
                                                                    href="">privacy policy</a>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="text-end col-12">
                                                        <div class="d-inline-block">
                                                            <button type="submit"
                                                                class="racf__btn d-flex align-items-center">
                                                                Signup Now
                                                                <img src="<?php echo $site_path ?>/images/arrow-link.svg"
                                                                    class="ms-10 svg" alt="">
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="have__account mt-30 text-center">
                                            Already have an Account? <a href="">Login</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '../../components/footer.php'; ?>
    <?php include '../../components/scripts.php'; ?>
</body>

</html>