<!DOCTYPE html>
<?php
$current_page = 'Search';
$timestamp = date("YmdHis");
$site_path = 'http://localhost/racf_website/assets';
$site_url = 'http://localhost/racf_website/views';
// $site_path = 'https://www.creamyw.com/dev/rent/web/assets';
// $site_url = 'https://www.creamyw.com/dev/rent/web/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../components/links.php'; ?>
</head>

<body>
    <?php include '../../components/menu.php'; ?>
    <div class="wrapper">
        <div class="not__found__page">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7 col-md-8 mx-auto text-center">
                        <div class="img">
                            <img src="https://www.racf.me/v1/application.static/assets/website/img/logos/logo_sad_m.png"
                                class="img-fluid" alt="">
                        </div>
                        <div class="title mt-40">
                            <h2>Page <span>Not</span> Found</h2>
                        </div>
                        <div class="desc mt-30">
                            Sorry, we can't find the page you're looking for.
                        </div>
                        <div class="mt-30 d-inline-block">
                            <a href="<?php echo $site_url ?>/home" class="racf__btn d-flex align-items-center">
                                Back Home
                                <img src="<?php echo $site_path ?>/images/arrow-link.svg" class="ms-10 svg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <?php include '../../components/footer.php'; ?> -->
    <?php include '../../components/sm-footer.php'; ?>
    <?php include '../../components/scripts.php'; ?>
</body>

</html>