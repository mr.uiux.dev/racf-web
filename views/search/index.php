<!DOCTYPE html>
<?php
$current_page = 'Search';
$timestamp = date("YmdHis");
$site_path = 'http://localhost/racf_website/assets';
$site_url = 'http://localhost/racf_website/views';
// $site_path = 'https://www.creamyw.com/dev/rent/web/assets';
// $site_url = 'https://www.creamyw.com/dev/rent/web/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>RACF | <?php echo $current_page ?></title>
    <?php include '../../components/links.php'; ?>
</head>

<body>
    <?php include '../../components/menu.php'; ?>
    <div class="wrapper">
        <div class="search__web__page">
            <div class="hobbies">
                <div class="container">
                    <div class="title text-center">
                        <h2>Find <span>CyberFriend</span> & Make Friend</h2>
                    </div>
                    <form action="" class="mt-lg-50 mt-md-40 mt-30">
                        <div class="form card p-lg-25 p-15 mt-md-30 mt-20">
                            <div class="pinboard__search">
                                <form action="">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="position-relative">
                                                <input type="search" placeholder="Find Cyber Friends from your pinboard"
                                                    name="" class="form-control" id="">
                                                <button type="submit" class="racf__btn">
                                                    <img src="<?php echo $site_path ?>/images/search.svg" alt="">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-12 mt-10">
                                            <div class="d-flex align-items-center flex-wrap">
                                                <div
                                                    class="filter__group d-flex align-items-center flex-wrap me-md-10 me-0 mb-md-0 mb-10">
                                                    <span class="me-10">Gender:</span>
                                                    <div class="actions">
                                                        <button class="racf__btn btn__action active">
                                                            Both
                                                        </button>
                                                        <button class="racf__btn btn__action">
                                                            Male
                                                        </button>
                                                        <button class="racf__btn btn__action">
                                                            Female
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="filter__group d-flex align-items-center flex-wrap">
                                                    <span class="me-10">Status:</span>
                                                    <div class="actions">
                                                        <button class="racf__btn btn__action active">
                                                            Both
                                                        </button>
                                                        <button class="racf__btn btn__action">
                                                            Online
                                                        </button>
                                                        <button class="racf__btn btn__action">
                                                            Offline
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="cards__profile mt-md-60 mt-50">
                                    <div class="row">
                                        <?php
                                        for ($i = 0; $i < 9; $i++) {
                                            echo '
                                                    <div class="col-lg-4 col-md-6">
                                                        <a href="' . $site_url . '/member/cyberprofile" class="d-block card__item position-relative">
                                                            <!-- online, offline -->
                                                            ' . (($i === 1) || ($i === 3) || ($i === 5)
                                                ? "<span class='status online'>Online</span>"
                                                : "<span class='status offline'>Offline</span>") . '
                                                            <div class="img text-center">
                                                                <img src="' . $site_path . '/images/profile.png"
                                                                    class="img-fluid" alt="">
                                                            </div>
                                                            <div class="name text-center mt-10 fw-bold">
                                                                Ahmed Mohamed
                                                            </div>
                                                            <div class="price text-center text-primary mt-10">
                                                                $1.03 per minute
                                                            </div>
                                                            <div class="about text-center mt-10">
                                                                I am 28 years old, I I\'m working as UX/UI Designer - Front End
                                                                Developer
                                                            </div>
                                                            <div class="tags mt-10">
                                                                <span>#football - $1 per/min</span>
                                                                <span>music</span>
                                                                <span>#video game - $1 per/min</span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    ';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- <?php include '../../components/footer.php'; ?> -->
    <?php include '../../components/sm-footer.php'; ?>
    <?php include '../../components/scripts.php'; ?>
</body>

</html>