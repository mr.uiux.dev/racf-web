$(() => {
	// SVG Converter
	$("img.svg").each(function () {
		var $img = jQuery(this);
		var imgID = $img.attr("id");
		var imgClass = $img.attr("class");
		var imgURL = $img.attr("src");
		jQuery.get(
			imgURL,
			function (data) {
				var $svg = jQuery(data).find("svg");
				if (typeof imgID !== "undefined") {
					$svg = $svg.attr("id", imgID);
				}
				if (typeof imgClass !== "undefined") {
					$svg = $svg
						.attr("class", imgClass + " replaced-svg")
						.css("display", "inline-block");
				}
				$svg = $svg.removeAttr("xmlns:a");
				$img.replaceWith($svg);
			},
			"xml"
		);
	});

	// Navbar Scroll
	const body = $("body");
	const navbarScroll = $(".navbar.nav__menu");
	$(window)
		.on("scroll", function () {
			if ($(this).scrollTop() > 0) {
				body.css({ paddingTop: navbarScroll.outerHeight() + "px" });

				navbarScroll.addClass("on__scroll");
			} else {
				body.css({ paddingTop: "0px" });

				navbarScroll.removeClass("on__scroll");
			}
		})
		.trigger("scroll");

	// open close menu
	const btnMenu = $("#btnMenu");
	const closeMenu = $("#closeMenu");
	const overlyMenu = $(".menu__overlay");
	const sideMenu = $(".nav__menu .links");
	btnMenu.on("click", () => {
		overlyMenu.addClass("active");
		sideMenu.addClass("active");
		closeMenu.addClass("active");
	});
	closeMenu.on("click", () => {
		closeMenu.removeClass("active");
		overlyMenu.removeClass("active");
		sideMenu.removeClass("active");
	});
});
