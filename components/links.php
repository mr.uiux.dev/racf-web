<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $site_path; ?>/favicon/apple-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $site_path; ?>/favicon/apple-icon-60x60.png" />
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $site_path; ?>/favicon/apple-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $site_path; ?>/favicon/apple-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $site_path; ?>/favicon/apple-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $site_path; ?>/favicon/apple-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $site_path; ?>/favicon/apple-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $site_path; ?>/favicon/apple-icon-152x152.png" />
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $site_path; ?>/favicon/apple-icon-180x180.png" />
<link rel="icon" type="image/png" sizes="192x192" href="<?php echo $site_path; ?>/favicon/android-icon-192x192.png" />
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $site_path; ?>/favicon/favicon-32x32.png" />
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $site_path; ?>/favicon/favicon-96x96.png" />
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $site_path; ?>/favicon/favicon-16x16.png" />
<link rel="manifest" href="<?php echo $site_path; ?>/favicon/manifest.json" />
<meta name="msapplication-TileColor" content="#ffffff" />
<meta name="msapplication-TileImage" content="<?php echo $site_path; ?>/favicon/ms-icon-144x144.png" />
<meta name="theme-color" content="#ffffff" />
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link
    href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;800;900&family=Roboto:wght@400;500;700&display=swap"
    rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $site_path; ?>/lib/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<link rel="stylesheet" href="<?php echo $site_path; ?>/css/racf-web.css?v=<?php echo $timestamp;?>" />