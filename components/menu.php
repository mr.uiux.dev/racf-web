<nav class="navbar nav__menu">
    <div class="container">
        <div class="d-flex align-items-center justify-content-between w-100">
            <div class="menu__left">
                <div class="d-flex align-items-center justify-content-lg-around justify-content-md-between">
                    <a href="" class="navbar-brand">
                        <img src="<?php echo $site_path ?>/images/logo.png" class="d-xl-block d-none" alt="">
                        <img src="<?php echo $site_path ?>/images/logo-sm.png" class="d-xl-none d-md-block d-none" alt="">
                        <img src="<?php echo $site_path ?>/images/logo-xs.png" class="d-xl-none d-md-none d-block" alt="">
                    </a>
                    <div class="links">
                        <a href="<?php echo $site_url ?>/home" class="nav-link <?php if ($current_page === "Home") echo 'active' ?>">Home</a>
                        <a href="<?php echo $site_url ?>/how-it-work" class="nav-link <?php if ($current_page === "How it Work") echo 'active' ?>">How it Works</a>
                        <a href="<?php echo $site_url ?>/hobbies" class="nav-link <?php if ($current_page === "Hobbies") echo 'active' ?>">Hobbies</a>
                        <a href="<?php echo $site_url ?>/faqs" class="nav-link <?php if ($current_page === "FAQs") echo 'active' ?>">FAQs</a>
                        <a href="<?php echo $site_url ?>/contact" class="nav-link <?php if ($current_page === "Contact") echo 'active' ?>">Contact</a>
                        <a href="<?php echo $site_url ?>/search" class="nav-link <?php if ($current_page === "Search") echo 'active' ?>">Search</a>
                    </div>
                </div>
            </div>
            <div class="menu__right d-flex align-items-center">
                <!-- If user not login -->
                <div class="text-end me-md-20 me-0 d-md-flex d-none">
                    <a href="<?php echo $site_url ?>/sginup" class="register__btn">Signup</a>
                    <a href="<?php echo $site_url ?>/login" class="racf__btn login__btn">Login</a>
                </div>
                <div class="dropdown dropdown__user__menu d-md-none d-block me-15">
                    <button class="racf__btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        <div class="d-flex flex-column">
                            <img src="<?php echo $site_path ?>/images/user-menu.png" alt="">
                        </div>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <!-- If user login on mobile view -->
                        <!-- <li class="user__login d-md-none d-block">
                            <div class="d-flex flex-column">
                                <span class="name">Mohamed Ramadan</span>
                                <span class="price">$2,200</span>
                            </div>
                        </li> -->
                        <li><a class="dropdown-item" href="<?php echo $site_url ?>/login">Login</a></li>
                        <li><a class="dropdown-item" href="<?php echo $site_url ?>/sginup">Signup</a></li>
                    </ul>
                </div>
                <!-- If user logined on Medium and Large view -->
                <!-- <div class="dropdown me-15">
                    <button class="racf__btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        <div class="d-flex flex-column">
                            <span class="name">Mohamed Ramadan</span>
                            <span class="price text-primary">$2,200</span>
                        </div>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item" href="#">Dashboard</a></li>
                        <li><a class="dropdown-item" href="#">How it works</a></li>
                        <li>
                            <hr>
                        </li>
                        <li><a class="dropdown-item" href="#">Logout</a></li>
                    </ul>
                </div> -->
                <button class="btn__menu d-lg-none d-md-flex align-items-center" id="btnMenu">
                    <img src="<?php echo $site_path ?>/images/menu-btn.svg" class="me-10 svg" alt="">
                    Menu
                </button>
            </div>
        </div>
        <button class="close__menu bg-transparent border-0 p-0" id="closeMenu">
            <img src="<?php echo $site_path ?>/images/close-menu.svg" alt="">
        </button>
        <div class="menu__overlay"></div>
    </div>
    </div>
</nav>