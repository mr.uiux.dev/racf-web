<script src="<?php echo $site_path; ?>/lib/jquery/jquery.min.js"></script>
<script src="<?php echo $site_path; ?>/lib/jquery-ui/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo $site_path; ?>/lib/mansory/imagesloaded.min.js"></script>
<script src="<?php echo $site_path; ?>/lib/mansory/mansory.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo $site_path; ?>/script/script.js"></script>