<footer class="sm__footer">
    <div class="container">
        <div class="footer__container">
            <div class="row">
                <div class="col-md-6">
                    <a class="footer-brand">
                        <img src="<?php echo $site_path ?>/images/logo.png" alt="">
                    </a>
                </div>
                <div class="col-md-6">
                    <div class="social text-end">
                        <a href="">
                            <i class="fa fa-envelope"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-envelope"></i>
                        </a>
                    </div>
                </div>
                <div class="col-12">
                    <div class="copyright text-center">
                        Copyright © 2012 - 2021, <a href="">RentaCyberFriend.com</a>. All Rights Reserved.
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>