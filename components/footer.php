<div class="get__started">
    <div class="container">
        <div class="data__btn p-lg-60 p-md-40 p-30">
            <div
                class="row align-item-center flex-lg-nowrap flex-wrap justify-content-lg-between justify-content-center">
                <div class="col-lg-8">
                    <div class="data text-lg-start text-center">
                        <div class="title">
                            <h2>Ready to start <span>RentaCyberFriend</span>?</h2>
                        </div>
                        <div class="desc">
                            Ready to start video call with friends by using RentaCyberFriend
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 text-lg-end text-center">
                    <div class="d-inline-block mt-lg-0 mt-30">
                        <a href="" class="racf__btn d-flex align-items-center">
                            Get Started
                            <img src="<?php echo $site_path ?>/images/arrow-link.svg" class="ms-10 svg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <a class="footer-brand">
                    <img src="<?php echo $site_path ?>/images/logo.png" alt="">
                </a>
                <div class="desc mt-20">
                    RentACyberFriend.com is a new meeting platform based on the concept of the pen pal, updated for
                    today’s digital and mobile world! Cyber Friends are offering a variety of services available to
                    Visitors/Users.
                </div>
            </div>
            <div class="col-lg-5 col-md-8 mt-lg-0 mt-30">
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-4 col-6">
                        <ul class="list-unstyled mb-0 links">
                            <li>
                                <a href="">Home</a>
                            </li>
                            <li>
                                <a href="">Hobbies</a>
                            </li>
                            <li>
                                <a href="">Contact</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-4 col-6">
                        <ul class="list-unstyled mb-0 links">
                            <li>
                                <a href="">How it Work</a>
                            </li>
                            <li>
                                <a href="">Features</a>
                            </li>
                            <li>
                                <a href="">Search</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-4 mt-md-0 mt-30">
                        <ul class="list-unstyled mb-0 links">
                            <li>
                                <a href="">FAQs</a>
                            </li>
                            <li>
                                <a href="">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="">Terns & Conditions</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-4 mt-lg-0 mt-30">
                <div class="text-white mb-30">Social Links</div>
                <div class="social">
                    <a href="">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="">
                        <i class="fa fa-envelope"></i>
                    </a>
                </div>
            </div>
            <div class="col-12">
                <hr class="hr my-30">
                <div class="copyright text-center">
                    Copyright © 2012 - 2021, RentaCyberFriend.com. All Rights Reserved.
                </div>
            </div>
        </div>
    </div>
</footer>